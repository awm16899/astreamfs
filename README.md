# AStreamFS and 1fichierFS

This repository contains two related fuse filesystems.

## AStreamFS:
Fuse filesystem to read http(s) URLs.  
The name stands for A(synchronous) Stream(ing) (fuse) F(ile)S(ystem)

This fuse filesystem can mount any http(s) link(s) and helps access them
as is they were local files.

The only limitation is that the server must be able to handles **ranges**.

There was already an httpfs implementation, but it was very naive:
doing a range request for each read request received.
That won't fly, especially with https! It was also not following location,
or not doing clever things like keeping locations for a given time.

This implementation also tries its best to stream the remote URL.
Unless accesses are completely random, in which case you will probably
be better of downloading the file... it will succeed quite well streaming
things since most tools we use do a lot of sequential reads and quite
few random accesses in between.

astreamfs version is 0.9.1 (as of November 2019) and there is still
some /TODO... see the TODO.md file.

### Examples of usage:

#### Without downloading it, look at an iso and display one of the files
(same example as the existing httpfs)

```shell
mkdir /tmp/ubuntu
mkdir /tmp/iso
astreamfs -JO http://releases.ubuntu.com/18.04/ubuntu-18.04.3-desktop-amd64.iso /tmp/ubuntu -o allow_other
sudo mount -o ro,loop /tmp/ubuntu/ubuntu-18.04.3-desktop-amd64.iso /tmp/iso
ls -l /tmp/iso
```

> total 78\
> dr-xr-xr-x  1 root root  2048 nov.   5 21:28 ./\
> drwxrwxrwt 20 root root   560 nov.  17 17:29 ../\
> dr-xr-xr-x  1 root root  2048 nov.   5 21:28 boot/\
> dr-xr-xr-x  1 root root  2048 nov.   5 21:28 casper/\
> dr-xr-xr-x  1 root root  2048 nov.   5 21:28 .disk/\
> dr-xr-xr-x  1 root root  2048 nov.   5 21:28 dists/\
> dr-xr-xr-x  1 root root  2048 nov.   5 21:28 EFI/\
> dr-xr-xr-x  1 root root  2048 nov.   5 21:28 install/\
> dr-xr-xr-x  1 root root 34816 nov.   5 21:28 isolinux/\
> -r--r--r--  1 root root 23882 nov.   5 21:29 md5sum.txt\
> dr-xr-xr-x  1 root root  2048 nov.   5 21:28 pics/\
> dr-xr-xr-x  1 root root  2048 nov.   5 21:28 pool/\
> dr-xr-xr-x  1 root root  2048 nov.   5 21:28 preseed/\
> -r--r--r--  1 root root   233 nov.   5 21:28 README.diskdefines\
> lr-xr-xr-x  1 root root     1 nov.   5 21:28 ubuntu -> ./

```shell
cat /tmp/iso/md5sum.txt
```

> cde56251d6cae5214227d887dee3bab7  ./pics/red-upperleft.png\
> 0730e775a72519aaa450a3774fca5f55  ./pics/red-lowerleft.png\
> \
> (...)\
> \
> 2404662e62695e894e85af67d202d05c  ./dists/bionic/main/binary-amd64/Packages.gz\
> f0fc0ff4ad6055c3a6ba5524de414fcd  ./install/mt86plus

```shell
sudo umount /tmp/iso
fusermount -u /tmp/ubuntu
rmdir /tmp/iso
rmdir /tmp/ubuntu
```


#### Unrar some file without having to donwload the chunks first
...even if they are located on different servers, verbosely logging
(debug level) to a file.

```shell
mkdir /tmp/chunks
astreamfs -JO https://example.com/foo.part01.rar https://example.com/foo.part02.rar https://another_host.com/foo.part03.rar -l7 --log-file=/tmp/debug.txt /tmp/chunks
ls -l /tmp/chunks
```
> -r--r--r--  1 user user 500000 nov.   5 21:29 foo.part01.rar\
> -r--r--r--  1 user user 500000 nov.   5 21:29 foo.part02.rar\
> -r--r--r--  1 user user 500000 nov.   5 21:29 foo.part03.rar

```shell
unrar e /tmp/chunks/foo.part01.rar
fusermount -u /tmp/chunks
rmdir /tmp/chunks
```

#### Remote cloud storage provider

Provided you have a **paid subscription** to a cloud file storage such as
1fichier, you could even simulate the same directory tree as your
remote storage, with the use of a (much) more complex command.
That is what the provided script `1fichier` does.
With the use of the 1fichiers "network identification" (identification
through your IP address, this will allow, for example to stream
holidays movies, or listen at music directly stored on the remote,
without having to donwload it first.

Of course you must have a fast enough connection according to the bitrate
of the media you stream, and stressing again: you MUST have a
**paid subscription**, otherwise you would be pretty limited by the
_free mode_ of such providers, and probably won't go past displaying
the listing of your remote!

This was actually how I accessed my remote storage before 1fichier.com
developed APIs, which lead to the easier to use and more feature-loaded
1fichierfs below.




## 1fichierfs
Is based on astreamfs but uses the API recently released by 1fichier.

This allows a much simpler command to mount: specifying only the mountpoint
is enough!

It also goes beyond what a simple http server can do with: renaming and
moving files and directory, hard linking, deleting files and directories,
creating directories, statistics, etc...

Note that you MUST have a **paid subscription** to 1fichier.com, because
the API is only accessible to paid customers. The first thing 1fichierfs
does is get the root directory of the remote, without a paid subscription
that will fail and 1fichierfs will exit.

### Examples of usage:

Since it is a derived work from astreamfs customised for 1fichier, it
can be tremendously simpler.

The shortest command is:

```shell
1fichierfs path_to_mountpoint
```

Yes, that's all you need. It will prompt for the API Key, and start
doing it's work.

Obviously, having to type the API key each time is impractical, reading
the help (`1fichierfs --help`) or the `man` provided with the package will
show all the options like statistics, refresh, etc...


## Packages:

On top of the current repository that allows compiling from source, there
are Ubuntu packages (LTS versions 16.04 and 18.04) on my PPA:

https://launchpad.net/~alainb06/+archive/ubuntu/astreamfs

For 1fichierfs, there are also packages for
**Raspberry Pi - Raspbian Buster 32 bits**

[1fichierfs_1.6.5~buster-1_armhf.deb](http://xslt2.0.free.fr/raspbian/1fichierfs_1.6.5~buster-1_armhf.deb)


## Français :

La page de manuel de 1fichierfs est aussi fournie en français.

Il y a un fil de forum français pour 1fichierfs.

https://forum.ubuntu-fr.org/viewtopic.php?id=2034345


## License GPL V3
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
