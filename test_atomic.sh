#! /bin/sh

# This returns the string -latomic if LLONG is not always atomic (2), or return an empty string otherwise

[ 2 -ne $( gcc -std=c11 -dM -E - </dev/null | sed -n '/ATOMIC_LLONG_LOCK_FREE/s/.*_FREE //p' ) ] && printf -- '-latomic'
