#
# 1fichierfs: non regression tests - Configuration documentation + default
#
# Copyright (C) 2018-2020  Alain BENEDETTI <alainb06@free.fr>
#
# License:
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

#===============
# PREAMBLE
#
# All the files in this directory, along with this one, will be sourced.

# This file serves 2 purposes:
# - Documentation (of the possible variables)
# - Default values (when possible)

# As usual, files are sourced in alphabetic order.
# So, to override a default variable defined in this file, instead of modifying
# this documentation file, just use a file with a higher number.
# For example: 01-my_variables.sh will override the variables defined here.
#===============



#================
# MUST DEFINE
#
# Those are the variables you MUST define.
# If they are left empty, the test will fail warning about empty variables and
# immediately exit without doing any tests.


# This is the root, relative to the mount root, of all the tests.
# This directory (full path) MUST exist at the root of the storage.
# Nothing will be written or changed in this directory.
# Do not include leading or trailing /
# Example: TEST_ROOT="foo"
TEST_ROOT=''

# This is a small file that MUST exist in the root above.
# Do not include leading or trailing /
# Example: TEST_SMALL_FILE="bar.txt"
TEST_SMALL_FILE=''

# This is the path where test will be run relative to the above root.
# This directory (full path) MUST exist in TEST_ROOT above.
# A temporary directory will be created inside this path.
# All tests will be done inside this temporary directory which will be deleted
#   in the end if all tests succeed.
# Otherwise the temporary directory and its content at the moment of the
#   failure will stay, so that the cause of the failure can be examined.
# Do not include leading or trailing /
# Example: TEST_PATH="Test"
TEST_PATH=''
#================



#================
# SHOULD DEFINE
#
# This is not per se MUST because 1fichierfs can do without --api-key
# But that means that the user will be prompt for the API Key at each run,
# on top of being cumbersome, it kills the test automation!
# So the user SHOULD really not left that variable empty. Look at 1fichierfs
# help or manual for the --api-key parameter. Main options are to directly
# set the API Key here, or put it in a file an address it starting with @

API_KEY=''
#================



#================
# REASONABLE DEFAULTS
#
# Those are reasonable default values.
# To change the values, proceed as documented above.

# If this value is non-empty, the test shell will cd to that directory prior
# to start any test.
# If this variable is defined, the target directory MUST exist.
# Default: WORKING_DIR=''  # Stay un the directory where the test is started.
WORKING_DIR=''

# This is the path, relative to WORKING_DIR if specified, where to mount
# for the test. It the path does not exist it is created.
# Do NOT put trailing /
# Put a leading / only to define an absolute path.
# The path is not removed after the test.
# Default: MOUNT_PATH='1fichier'
MOUNT_PATH='1fichier'

# This is the executable to launch
# Defaults to 1fichierfs, assuming it has been installed somewhere in the
# directories specified in the $PATH
# To test a patched and compiled 1fichierfs, just put the full path (or path
# relative to WORKING_DIR) in the variable.
# Default: EXEC='1fichierfs'
EXEC='1fichierfs'

# Use Valgrind
# The default is to use valgrind if it is installed.
# To prevent using valgrind if you have it installed but don't want to use
# it for the test, define this variable to any non empty value.
# Default: USE_VALGRIND='' # means use Valgrind if it is installed!
USE_VALGRIND=''

# Log level
# The default is 7 since it is for testing/debugging
# Change it to a lower level if needed.
# Default: LOG_LEVEL='7'
LOG_LEVEL='7'

# Log file pattern
# The pattern should include a %d (printf like) that will be replaced by the
# number of the test set. This will allow to have a seperate debug file for
# each test set and pinpoint bugs in all sets.
# If the pattern does not include %d, the log file will be the same for all
# test sets, each new test set overwriting the debug file.
# If this variable is left empty there will be no log file. Bear in mind that
# at verbosity level 7 there will be really a lot of output, and unless
# additional options specify -d or -f, that could overflood the system log.
# Default: LOG_FILE_PATTERN='debug%d.txt'
LOG_FILE_PATTERN='debug%d.txt'

# stat filename
# Leave it blank for no stat file, but that means the protection tests on
# the stat file will NOT be run. So it is advised to define it for test
# completion, or leave the default if it is fine.
# Default: STAT_FILE='.stats'
STAT_FILE='.stats'

# refresh filename
# Leave it blank for no refesh file, but that means the protection tests on
# the refresh file will NOT be run. So it is advised to define it for test
# completion, or leave the default if it is fine.
# Default: REFRESH_FILE="!000_refresh.txt"
REFRESH_FILE="!000_refresh.txt"

# other options
# By default there are no other options.
# Although it might be useful to add -4 if another instance of 1fichierfs is
# already running on the same network, or options like --ftp-user=whatever
# to isolated and check the work done by the test script.
# Separate each option by a space.
# Default: OTHER_OPTIONS=''
OTHER_OPTIONS=''

# Run tests sets // TODO future feature
# By default will run all tests sets.
# -1st set is ran adding --raw-names to the options
# -2nd set is ran without additional options
# -3rd set is ran with --raw-names and with the remote root as TEST_ROOT
# Put the numbers of the tests that should be run in the variable.
# Note that tests are always ran in the same order, whatever is the order of
# numbers in the variable.
# Some tests inside the sets can be skipped if all sets are not selected.
# Default: RUN_SETS='1 2 3'
# //TODO: for the moment this is not taken into account
#RUN_SETS='1 2 3'

# Size (in Megabytes) of the random upload file.
# This file will be randomly generated at startup and uploaded.
# If the default 128MB takes too much time for the network used, this variable
# can be changed. It can also be higher to test bigger files (although the
# random generation will then take more time).
# Default: BIG_FILE_SIZE=128
BIG_FILE_SIZE=128


# Wheter to test removal of upload directory and related features.
# Since this test can interfere with another running instance of 1fichierfs
# the default value ('') is not to do the test.
# Note that if the upload environment is not found (lazy) when the test starts
# interferences are less problematic, hence the test will be done whatever the
# value of this variable. So this variable only has an effect when the
# upload environment is already present. If the value is anything not empty
# it means yes, the test will be done.
# Default: '' (don't do that test)
RM_UPLOAD_DIR_TEST=''
#================


#================
# DO NOT CHANGE (unless you understand what you do and have a perfectly good
#                reasons to change!)
#

# This is the time to wait (seconds) for upload to be stored.
# It takes into account how 1fichier.com works, inter alia the 5 minutes wait
# after the completion of the upload, a fixed time, and a time depending on
# the size of the file (above).
# It is advised NO TO CHANGE this variable, the risk being that the uploaded
# file won't be stored + ready and the comparison test will fail.
# Default: WAIT_TIME=420
WAIT_TIME=420
