# astreamfs and 1fichierfs /TODO
Fuse filesystem to read http(s) URL

## Priority code clean:
- Isolate common code between both developments to avoid duplication and
  facilitate bug fix in the 2 programs.

## Enhancements:
- astreamfs: Possibly better algorithm for directory list
- astreamfs: Performance: sort and bsearch on filenames instead of
             sequential search

- 1fichierfs: better writing algo to avoid double copy when receiving big writes


## Nice to have
- astreamfs Arguments -K (config file) option, especially for very long list
  of URLs
- some statistics on top or request timings
- Review the "buffer" algo and enchance it to remove locks and make it better
  for random reads.
- 1fichierfs: need a real json parser instead of strstr!
- 1fichierfs: implement truncate (case truncate to zero) //This is very complex
  for little benefit. It would require merging live read and write files in
  memory for atomicity. Workaround: the user can 1st delete then write. So it
  is moved to Nice to have
- Replace semaphore with futex


## Not decided:
- possibly cache in a file: list of URL => size/modification time/names
  to reuse between runs (goes along with -K, or keep it with shell script?)
- Issue with REALTIME since it can change when the clock changes,
  this might be gone with futex that work with an interval.
- We could do some timing computation to guess the best thing between
  opening a new stream or reading/discarding gaps.

## Continue to clean the code!
